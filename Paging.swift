//
//  Paging.swift
//  PaingMoreUsingGeneratorType
//
//  Created by SungJaeLEE on 2016. 12. 1..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import Foundation
import UIKit
//MARK: Generator

protocol AsyncGeneratorType {
    associatedtype Element
    associatedtype Fetch
}
extension AsyncGeneratorType {
    func next(fetchNextBatch: Fetch, onFinish: ((Element) -> Void)?) {
        
    }
}

class PagingGenerator<T>: AsyncGeneratorType
{
    typealias Element = Array<T>
    typealias Fetch = (Int, Int, (Element) -> Void) -> Void


    func aa(a:Int) {
        
    }
    
    var offset:Int
    let limit: Int
    
    init(offset: Int = 0, limit: Int = 25) {
        self.offset = offset
        self.limit = limit
    }
    
    func next(fetchNextBatch: Fetch, onFinish: ((Element) -> Void)?) {
        fetchNextBatch(offset, limit) { [unowned self] (items) in
            onFinish?(items)
            self.offset += items.count
        }
    }
    
}
