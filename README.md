# README #

Swift 언어에는 흥미로는 Protocol이 있습니다. 바로 SequenceType 인데요. 
말 그대로 순차적인 행위를 할수 있게 해줍니다.배열이나 컬렉션이 순차적인 행위를 할수 있는 것도 바로 이 때문이지요.   
그런데 이것의 Next 행위에 대해서 GeneratorType.Next 라는 것을 사용합니다.  
이곳에서 다음 Element를 가지고 오게 구현하면 Custom Type에도 순차 행위구현이 가능해집니다. 

Swift 3 에서는 IteratorType으로 이름이 바뀌였더군요. 
암튼 Back-End 와 연동하여 프로그래밍을 할때 필연적으로 Query 시에 Limit을 걸어주고 Cursor를 받아와서 NextFetch를 구현해야 하는 
경우(Pagination)가 상당히 많이 있습니다. 어찌보면 이 행위는 멀리서 보면 SequenceType을 따라줘서 NextFetch를 구현하는 것과 상당히 흡사합니다. 
따라서 SequenceType를 비슷하게 구현하여 쉽게 Pagination API를 구현해 보았습니다. :) 

사용법

fetchNextBatch 부분에서 fetch 하고 잘받아오면 Completion 넘겨주고 
onFinish 부분에서 Model Update를 해주시면 됩니다. 


```
#!Swift
paging.next(fetchNextBatch: { (offset, limit, completion) in
  if let remotelyFetched = self.downloadGithubUsers(offset: offset) {
      completion(remotelyFetched)
  }
}, onFinish: updateDataSource)

```