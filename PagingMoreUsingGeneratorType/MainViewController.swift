//
//  ViewController.swift
//  PaingMoreUsingGeneratorType
//
//  Created by SungJaeLEE on 2016. 12. 1..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    private let cellId = "cellId"
    
     var contacts = [Contact]() {
        didSet {
            tableView.reloadData()
        }
    }
    fileprivate var paging = PagingGenerator<Contact>(offset: 0, limit: 5)
    
    
    let spinner: UIActivityIndicatorView = {
       let spv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        //spv.hidesWhenStopped = true
        spv.startAnimating()
        return spv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.register(MainCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = spinner
        
        paging.next(fetchNextBatch: { (offset, limit, completion) in
            if let remotelyFetched = self.downloadGithubUsers(offset: offset) {
                //spinner.stopAnimating()
                completion(remotelyFetched)
            } else {
                spinner.stopAnimating()
            }

        }, onFinish: updateDataSource)
    }
    
    private func updateDataSource(items: Array<Contact>) {
        self.contacts.append(contentsOf: items)
    }

    func fetchNextBatch(offset: Int, limit: Int, completion: (Array<Contact>) -> Void) -> Void {
        if let remotelyFetched = self.downloadGithubUsers(offset: offset) {
            completion(remotelyFetched)
        }
    }
    
    func downloadGithubUsers(offset: Int) -> [Contact]? {
        print("offset : \(offset) ")
        var fetched = [Contact]()
        let pageNum:Int = offset / paging.limit
        if let url = NSURL(string: "https://api.github.com/users/vojtajina/followers?page=\(pageNum)&per_page=\(paging.limit)"),
            let data = NSData(contentsOf: url as URL)
        {
            let parsedObject: Any?
            do {
                parsedObject = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments)
            } catch _ as NSError {
                parsedObject = nil
            }
            if let users = parsedObject as? NSArray {
                for user in users {
                    if let dict = user as? NSDictionary, let login = dict["login"] as? String {
                        fetched.append(Contact(firstName:login, lastName:""))
                    }
                }
            }
        }
        return fetched.count > 0 ? fetched : nil
    }

    // TableViewDataSource..............
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MainCell
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = "\(contact.firstName) \(contact.lastName)"
        return cell
    }
    
    // TableViewDelegate..................
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.dataSource!.tableView(tableView, numberOfRowsInSection: indexPath.section) - 1 {
            
            paging.next(fetchNextBatch: fetchNextBatch, onFinish: updateDataSource)
        }

    }
}

class MainCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    func setupViews() {
        //backgroundColor = .red
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


